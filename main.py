from ova import *
import os
import matplotlib.pyplot as plt
import numpy as np
from uuid import uuid4
import shutil
from SVMColorClassifier import SVMColorClassifier


def crop_image(image_path):
    image = Image.open(image_path)
    width, height = image.size
    bottom_half = image.crop((0, 120, width, height))
    if bottom_half.getbbox() is not None:
        bottom_half.save(image_path)
    else:
        print('Error: Could not crop image')


def play_congratulations(melody=None):
    if melody is None:
        return

    for note in melody:
        robot.playMelody([note])


color_guesser = IncredibleColorGuesser()
svm = SVMColorClassifier()

color = {
    'red': {'r': 255, 'g': 0, 'b': 0},
    'yellow': {'r': 255, 'g': 255, 'b': 0},
    'green': {'r': 0, 'g': 255, 'b': 84},
    'cyan': {'r': 0, 'g': 255, 'b': 255},
    'magenta': {'r': 254, 'g': 0, 'b': 255},
    'blue_images': {'r': 0, 'g': 0, 'b': 255},
    'black_images': {'r': 0, 'g': 0, 'b': 0},
}

music = {
    'red': [(261, 100)],
    'yellow': [(293, 100)],
    'green': [(329, 100)],
    'cyan': [(349, 100)],
    'magenta': [(391, 100)],
    'blue_images': [(440, 100)],
    'black_images': [(0, 100)],
}

prev_prediction = None

robot: IRobot = OvaClientMqtt(id="ova1097bdcb50d5",
                              arena="ishihara",
                              username="demo",
                              password="demo",
                              server="192.168.10.103",
                              port=1883,
                              imgOutputPath='images/img.jpg'
                              )

print(pyfiglet.figlet_format("Bienvenue !"))
choice = input("Tapez '1' pour train le SVM ou '2' pour le mode prédicition: ")

if choice == '1':
    svm.train()
    print("Entraintemen du SVM terminé !")

elif choice == '2':
    while True:
        robot.update()
        os.system('clear')

        if not robot.isConnected():
            print("🔴", robot.getRobotId(), "déconnectée")

        else:
            imageLargeur = robot.getImageWidth()
            imageHauteur = robot.getImageHeight()

            crop_image('images/img.jpg')

            prediction = svm.predict('images/img.jpg')

            if prediction != prev_prediction and prediction != 'void':
                rgb = color[prediction]
                robot.setLedColor(rgb['r'], rgb['g'], rgb['b'])
                prev_prediction = prediction
                play_congratulations(music[prediction])
                print("Nouvelle prédicition: ", prediction)
