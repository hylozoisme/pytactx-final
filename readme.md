# Cahier des charges 

## Introduction
Quand on run, on veut un prompt : 
- soit entrainement
- soit challenge


# Le test d’Ishihara 🎨
## Introduction
🎯 Votre mission si vous l’acceptez
<br/>
🎥 Sera de reconnaitre une série de couleurs à l’aide de la camera d’Ova
<br/>
💡 Et d’afficher la couleur reconnue avec sa LED RGB embarquée
<br/>
🎨 Réussirez-vous à reconnaître toutes les couleurs ?


<br/>Niv 1 : séquence couleur à reconnaitre aléatoire durée fixe, nb
couleur palette connu et fixé à 4, req led sur chaque nouvelle
couleur 
<br/>Niv 2 : 6 couleurs palette, durée variable non connue à l’avance
<br/>Niv 3 : prompt table transfert couleur - freq buzzer au démarrage,
req note + led